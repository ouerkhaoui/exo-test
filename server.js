var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');


var app = express();

app.use(express.static(path.resolve(__dirname, './client')));
app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: false }));


app.get('/', (req, res) => {
  res.sendfile('./public/index.html');
});

app.get('/api/restApi', (req, res) => {

  res.send({
    "question":"Aimez-vous Angular ?",
    "oui":"Bon choix !",
    "non":"Zut !"
  });
});


app.listen(3000, () => console.log('Exo test listening on port 3000!'));