

app.controller('MainCtrl',['$scope', 'restApi', function($scope, restApi) {
	
	$scope.title = "BeMore Consulting TEST.";

	restApi.getRestApi(function(res, err) {$scope.result = res.data});

	//Masquer les alerts
	$("#reponse-oui").hide();
	$("#reponse-non").hide();

	//Afficher une alert contenant notre réponse à la réponse du client si OUI ou NON
	function reponseChoisie(reponse) {
		if(reponse == 'oui'){
			$("#reponse-oui").show();
			$("#reponse-non").hide();

			setTimeout(function(){ $("#reponse-oui").hide() }, 1500);
		} else if(reponse == "non") {
			$("#reponse-oui").hide();
			$("#reponse-non").show();

			setTimeout(function(){ $("#reponse-non").hide() }, 1500);
		} else {
			$("#reponse-oui").hide();
			$("#reponse-non").hide();
		}
	}

	//Evenement click sur le bouton OUI
	$("#question-oui").on('click', function () {reponseChoisie("oui")});

	//Evenement click sur le bouton NON
	$("#question-non").on('click', function () {reponseChoisie("non")});
}]);