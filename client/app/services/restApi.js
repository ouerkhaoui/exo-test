

app.factory('restApi', ["$http", function ($http) {
  return {
    getRestApi: function (cb) {
      $http.get("/api/restApi")
        .then(function (results) {
          cb(results, null);
        }, function (error) {
          cb(null, error);
        });
    }
  }
}]);